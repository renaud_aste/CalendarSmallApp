package com.aste.smallapp.calendar.helpers;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.CalendarContract;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;

import com.aste.smallapp.calendar.beans.UserCalendar;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;

import java.util.TimeZone;

/**
 * CalendarSmallApp - Builder for event.
 */
public class EventBuilder {

    private Context context;

    private UserCalendar calendar;

    private String title;

    private boolean allDay;

    private DateTime start;

    private DateTime end;

    private Long eventId;

    /**
     * Construct the EventBuilder.
     *
     * @param context
     */
    public EventBuilder(Context context) {
        Validate.notNull(context, "Context should not be null in EventBuilder Constructor.");
        this.context = context;
    }

    /**
     * Fill data of EventBuilder.
     *
     * @param calendar  the calendar
     * @param etTitle   the event title EditText.
     * @param allDay    the allDay Checkbox.
     * @param start     the start date.
     * @param end       the end date.
     */
    public void populateBuilder(final UserCalendar calendar, final EditText etTitle,
                                final CheckBox allDay, final DateTime start,
                                final DateTime end) {
        Validate.notNull(calendar, "UserCalendar should not be null in EventBuilder Constructor.");
        Validate.notNull(etTitle, "EditText should not be null in EventBuilder Constructor.");
        Validate.notNull(allDay, "All day should not be null in EventBuilder Constructor.");
        Validate.notNull(start, "Start date should not be null in EventBuilder Constructor.");
        Validate.notNull(end, "End date should not be null in EventBuilder Constructor.");

        this.calendar = calendar;
        this.title = etTitle.getText().toString();
        this.allDay = allDay.isChecked();
        this.start = start;
        this.end = end;
    }

    /**
     * Create the event.
     *
     * @return the created event id.
     */
    public void createEvent() {
        Validate.notNull(this.calendar, "Calendar should not be null when creating event.");
        Validate.notNull(this.title, "Title should not be null when creating event.");
        Validate.notNull(this.start, "Start date should not be null when creating event.");
        Validate.notNull(this.end, "End date should not be null when creating event.");
        Validate.notNull(this.allDay, "All day should not be null when creating event.");

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.CALENDAR_ID, this.calendar.getId());
        values.put(CalendarContract.Events.TITLE, this.title);
        values.put(CalendarContract.Events.DTSTART, this.start.getMillis());
        values.put(CalendarContract.Events.DTEND, this.end.getMillis());
        values.put(CalendarContract.Events.ALL_DAY, this.allDay);
        values.put(CalendarContract.Events.ORGANIZER, this.calendar.getOwnerName()); //TODO email here
        values.put(CalendarContract.Events.IS_ORGANIZER, 1);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());

        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        if (NumberUtils.isNumber(uri.getLastPathSegment())) {
            this.eventId = Long.valueOf(uri.getLastPathSegment());
        } else {
            Log.e(EventBuilder.class.getSimpleName(), "Could not set eventId (not a number).");
        }
    }

    public void addAttendee(final String name, final String email) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Attendees.ATTENDEE_NAME, name);
        values.put(CalendarContract.Attendees.ATTENDEE_EMAIL, email);
        values.put(CalendarContract.Attendees.ATTENDEE_RELATIONSHIP, CalendarContract.Attendees.RELATIONSHIP_ATTENDEE);
        values.put(CalendarContract.Attendees.ATTENDEE_TYPE, CalendarContract.Attendees.TYPE_NONE);
        values.put(CalendarContract.Attendees.ATTENDEE_STATUS, CalendarContract.Attendees.SELF_ATTENDEE_STATUS);
        values.put(CalendarContract.Attendees.EVENT_ID, eventId);
        Uri uri = cr.insert(CalendarContract.Attendees.CONTENT_URI, values);
    }

    public void addReminder(Integer minutes) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Reminders.MINUTES, minutes);
        values.put(CalendarContract.Reminders.EVENT_ID, eventId);
        values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        Uri uri = cr.insert(CalendarContract.Reminders.CONTENT_URI, values);
    }

    /**
     * Show the event in the regular calendar application.
     */
    public void showEvent() {
        Validate.notNull(eventId, "Impossible to show event if eventId is null.");

        Uri uri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, this.eventId);
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Constants.INTENT_BEGIN_TIME, this.start.getMillis());
        intent.putExtra(Constants.INTENT_END_TIME, this.end.getMillis());
        context.startActivity(intent);
    }

    /**
     * Getter for eventId.
     *
     * @return the event id.
     */
    public long getEventId() {
        return eventId;
    }
}
