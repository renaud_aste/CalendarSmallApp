package com.aste.smallapp.calendar.helpers;

/**
 * CalendarSmallApp - Constants.
 */
public final class Constants {

    /**
     * Intent Extra Key for Calendar Start Date.
     */
    public static String INTENT_BEGIN_TIME = "beginTime";

    /**
     * Intent Extra Key for Calendar End Date.
     */
    public static String INTENT_END_TIME = "endTime";
}
