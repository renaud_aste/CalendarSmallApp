package com.aste.smallapp.calendar.listeners;

import org.joda.time.DateTime;

/**
 * Created by Renaud on 19/08/2015.
 */
public interface DateCallback {

    void onDateTimeSet(final DateTime dateTime);
}
