package com.aste.smallapp.calendar.listeners;

import android.app.TimePickerDialog;
import android.widget.Button;
import android.widget.TimePicker;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;

/**
 * Created by renaud on 09/07/15.
 */
public class OnTimeSetListenerCustom implements TimePickerDialog.OnTimeSetListener {

    /**
     * Le callback de la saisie de la date.
     */
    private DateCallback callback;

    /**
     * La date que l'on est en train de renseigner
     */
    private DateTime dateTime;

    public OnTimeSetListenerCustom(final DateCallback callback, final DateTime dateTime) {
        this.callback = callback;
        this.dateTime = dateTime;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        callback.onDateTimeSet(dateTime.withHourOfDay(hourOfDay).withMinuteOfHour(minute));
    }
}
