package com.aste.smallapp.calendar;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.aste.smallapp.calendar.beans.UserCalendar;
import com.aste.smallapp.calendar.helpers.DateHelper;
import com.aste.smallapp.calendar.helpers.EventBuilder;
import com.aste.smallapp.calendar.helpers.SystemHelper;
import com.aste.smallapp.calendar.helpers.ViewHelper;
import com.aste.smallapp.calendar.listeners.DateCallback;
import com.aste.smallapp.calendar.listeners.OnCheckBoxListener;
import com.aste.smallapp.calendar.listeners.OnDateSetListenerCustom;
import com.aste.smallapp.calendar.listeners.OnTimeSetListenerCustom;
import com.sony.smallapp.SmallAppWindow;
import com.sony.smallapp.SmallApplication;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

/**
 * CalendarSmallApp - Sony Small App
 */
public class CalendarSmallApp extends SmallApplication implements View.OnClickListener {

    /**
     * Spinner for calendar list.
     */
    private Spinner spCalendar;

    /**
     * Field to set event title.
     */
    private EditText etTitle;

    /**
     * Checkbox for "All day".
     */
    private CheckBox cbAllDay;

    /**
     * Start date picker button.
     */
    private Button btDatePickerStart;

    /**
     * Start time picker button.
     */
    private Button btTimePickerStart;

    /**
     * End date picker button.
     */
    private Button btDatePickerEnd;

    /**
     * End time picker button.
     */
    private Button btTimePickerEnd;

    /**
     * Event start time.
     */
    private DateTime dateTimeStart = new DateTime();

    /**
     * Event end time.
     */
    private DateTime dateTimeEnd = new DateTime();

    /**
     * Button to create the event.
     */
    private Button btAdd;

    /**
     * Button to create and show the event.
     */
    private Button btAddShow;

    /**
     * Flag to check if event is all day or not.
     */
    private boolean is24hour;

    @Override
    protected void onCreate() {
        super.onCreate();
        this.initSmallApp();
        this.initViews();

        this.populateCalendars();
        SystemHelper.showKeyboard(this);
    }

    /**
     * Initialize Small App.
     */
    private void initSmallApp() {
        this.setTitle(StringUtils.EMPTY);
        this.setContentView(R.layout.main);
        this.setMinimizedView(R.layout.minimized);
        this.is24hour = DateFormat.is24HourFormat(CalendarSmallApp.this);

        SmallAppWindow.Attributes attr = this.getWindow().getAttributes();
        attr.width = this.getResources().getDimensionPixelSize(R.dimen.width);
        attr.height = this.getResources().getDimensionPixelSize(R.dimen.height);
        attr.flags = 0;
        attr.flags |= SmallAppWindow.Attributes.FLAG_NO_TITLEBAR;
        attr.flags |= SmallAppWindow.Attributes.FLAG_HARDWARE_ACCELERATED;
        this.getWindow().setAttributes(attr);
        this.getWindow().setBackgroundDrawable(this.getResources().getDrawable(R.drawable.light));
    }

    /**
     * Initialize views.
     */
    private void initViews() {
        this.etTitle = (EditText) findViewById(R.id.et_title);
        this.btDatePickerStart = (Button) this.findViewById(R.id.bt_date_picker_start);
        this.btTimePickerStart = (Button) this.findViewById(R.id.bt_time_picker_start);
        this.btDatePickerEnd = (Button) this.findViewById(R.id.bt_date_picker_end);
        this.btTimePickerEnd = (Button) this.findViewById(R.id.bt_time_picker_end);
        this.cbAllDay = (CheckBox) findViewById(R.id.cb_all_day);
        this.cbAllDay.setOnCheckedChangeListener(new OnCheckBoxListener(btTimePickerStart, btTimePickerEnd));
        this.spCalendar = (Spinner) this.findViewById(R.id.spinner);
        this.btAdd = (Button) findViewById(R.id.bt_add);
        this.btAdd.setOnClickListener(this);
        this.btAddShow = (Button) findViewById(R.id.bt_add_show);
        this.btAddShow.setOnClickListener(this);
        this.initDateTimePickers();
    }

    private void performClick(final Button button) {
        if (!cbAllDay.isChecked()) {
            button.performClick();
        }
    }

    /**
     * Initialize date picker and time pickers.
     */
    private void initDateTimePickers() {
        this.setButtonLabel();

        this.btDatePickerStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemHelper.hideKeyboard(CalendarSmallApp.this, etTitle);
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        CalendarSmallApp.this,
                        new OnDateSetListenerCustom(new DateCallback() {
                            @Override
                            public void onDateTimeSet(DateTime dateTime) {
                                dateTimeStart = dateTime;
                                setButtonLabel();
                                performClick(btTimePickerStart);
                            }
                        }, dateTimeStart),
                        dateTimeStart.getYear(),
                        dateTimeStart.getMonthOfYear(),
                        dateTimeStart.getDayOfMonth());
                datePickerDialog.show();
            }
        });

        this.btDatePickerEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemHelper.hideKeyboard(CalendarSmallApp.this, etTitle);
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        CalendarSmallApp.this,
                        new OnDateSetListenerCustom(new DateCallback() {
                            @Override
                            public void onDateTimeSet(DateTime dateTime) {
                                dateTimeEnd = dateTime;
                                setButtonLabel();
                                performClick(btTimePickerEnd);
                            }
                        }, dateTimeEnd),
                        dateTimeEnd.getYear(),
                        dateTimeEnd.getMonthOfYear(),
                        dateTimeEnd.getDayOfMonth());
                datePickerDialog.show();
            }
        });


        this.btTimePickerStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemHelper.hideKeyboard(CalendarSmallApp.this, etTitle);
                TimePickerDialog timePickerDialog = new TimePickerDialog(
                        CalendarSmallApp.this,
                        new OnTimeSetListenerCustom(new DateCallback() {
                            @Override
                            public void onDateTimeSet(DateTime dateTime) {
                                dateTimeStart = dateTime;
                                setButtonLabel();
                            }
                        }, dateTimeStart),
                        dateTimeStart.getHourOfDay(),
                        dateTimeStart.getMinuteOfHour(),
                        is24hour
                );
                timePickerDialog.show();
            }
        });

        this.btTimePickerEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemHelper.hideKeyboard(CalendarSmallApp.this, etTitle);
                TimePickerDialog timePickerDialog = new TimePickerDialog(
                        CalendarSmallApp.this,
                        new OnTimeSetListenerCustom(new DateCallback() {
                            @Override
                            public void onDateTimeSet(DateTime dateTime) {
                                dateTimeEnd = dateTime;
                                setButtonLabel();
                            }
                        }, dateTimeEnd),
                        dateTimeEnd.getHourOfDay(),
                        dateTimeEnd.getMinuteOfHour(),
                        is24hour
                );
                timePickerDialog.show();
            }
        });
    }

    /**
     * Set all date and time button labels (with selected dates).
     */
    private void setButtonLabel() {
        java.text.DateFormat formatDate = DateFormat.getDateFormat(this);
        btDatePickerStart.setText(formatDate.format(dateTimeStart.toDate()));
        btDatePickerEnd.setText(formatDate.format(dateTimeEnd.toDate()));

        java.text.DateFormat formatTime = DateFormat.getTimeFormat(this);
        btTimePickerStart.setText(formatTime.format(dateTimeStart.toDate()));
        btTimePickerEnd.setText(formatTime.format(dateTimeEnd.toDate()));
    }

    /**
     * Create the event through the EventBuilder.
     *
     * @return the event builder.
     */
    private EventBuilder createEvent() {
        UserCalendar calendar = (UserCalendar) spCalendar.getSelectedItem();
        EventBuilder eventBuilder = new EventBuilder(CalendarSmallApp.this);
        eventBuilder.populateBuilder(calendar, etTitle, cbAllDay, dateTimeStart, dateTimeEnd);
        eventBuilder.createEvent();
        eventBuilder.addReminder(15);
        return eventBuilder;
    }

    /**
     * Populate the Calendar Spinner.
     */
    private void populateCalendars() {
        CalendarListProvider calendarListProvider = new CalendarListProvider(this);
        List<UserCalendar> calendars = calendarListProvider.getCalendars();

        CalendarAdapter adapter = new CalendarAdapter(this, calendars);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spCalendar.setAdapter(adapter);
    }

    /**
     * Check if is form is valid.
     *
     * @return true if form is valid, else false.
     */
    private boolean isFormValid() {
        List<String> errors = new ArrayList<>();

        if (StringUtils.isNotBlank(etTitle.getText())) {
            etTitle.setHintTextColor(color(android.R.color.white));
        } else {
            etTitle.setHintTextColor(color(android.R.color.holo_red_light));
            errors.add(getString(R.string.invalid_form_event));
        }

        if (this.isDateValid()) {
            ViewHelper.setTextViewColor(color(android.R.color.white), btDatePickerStart, btDatePickerEnd, btTimePickerStart, btTimePickerEnd);
        } else {
            ViewHelper.setTextViewColor(color(android.R.color.holo_red_light), btDatePickerStart, btDatePickerEnd, btTimePickerStart, btTimePickerEnd);
            errors.add(getString(R.string.invalid_form_date));
        }

        boolean isValid = errors.isEmpty();
        if (!isValid) {
            Toast.makeText(this, StringUtils.join(errors, "\n"), Toast.LENGTH_SHORT).show();
        }
        return isValid;
    }

    /**
     * Check all date/time fiels validity?
     * @return true if valid, false otherwise.
     */
    private boolean isDateValid() {
        boolean valid = dateTimeStart.withSecondOfMinute(0).isBefore(dateTimeEnd.withSecondOfMinute(0));
        if (cbAllDay.isChecked()) {
            LocalDate start = dateTimeStart.toLocalDate();
            LocalDate end = dateTimeEnd.toLocalDate();
            valid = start.isBefore(end) || start.isEqual(end);
        }
        return valid;
    }

    /**
     * Get the color fom resources.
     *
     * @param color the color to get.
     * @return the color id.
     */
    private int color(int color) {
        return this.getResources().getColor(color);
    }


    @Override
    public void onClick(View v) {
        if (isFormValid()) {
            switch (v.getId()) {
                case R.id.bt_add:
                    CalendarSmallApp.this.createEvent();
                    Toast.makeText(this, this.sumUpEvent(), Toast.LENGTH_LONG).show();
                    CalendarSmallApp.this.finish();
                    break;
                case R.id.bt_add_show:
                    EventBuilder eventBuilder = CalendarSmallApp.this.createEvent();
                    if (eventBuilder != null) {
                        eventBuilder.showEvent();
                    }
                    CalendarSmallApp.this.finish();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Sum up event in one String.
     * @return the event sumup.
     */
    private String sumUpEvent() {
        String result;

        String title = this.etTitle.getText().toString();
        if (this.cbAllDay.isChecked()) {
            String start = DateHelper.printDate(this, dateTimeStart);
            String end = DateHelper.printDate(this, dateTimeEnd);
            result = title + "\n" + start + "\n" + end;
        } else {
            String start = DateHelper.printDateTime(this, dateTimeStart);
            String end = DateHelper.printDateTime(this, dateTimeEnd);
            result = title + "\n" + start + "\n" + end;
        }
        return result;
    }


}
