package com.aste.smallapp.calendar.helpers;

import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.aste.smallapp.calendar.CalendarSmallApp;
import com.sony.smallapp.SmallApplication;

/**
 * Helper class to help interaction with Android system.
 */
public final class SystemHelper {

    /**
     * Show keyboard.
     *
     * @param app the small app
     */
    public static void showKeyboard(final SmallApplication app) {
        app.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    /**
     * Hide keyboard.
     *
     * @param context the context
     */
    public static void hideKeyboard(final Context context, final View view) {
        final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
