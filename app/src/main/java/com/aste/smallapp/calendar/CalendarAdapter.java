package com.aste.smallapp.calendar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aste.smallapp.calendar.beans.UserCalendar;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * CalendarSmallApp - Custom adapter to show calendars in spinner.
 */
public class CalendarAdapter extends ArrayAdapter<UserCalendar> {

    public CalendarAdapter(Context context, List<UserCalendar> users) {
        super(context, 0, users);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return this.getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return this.getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        UserCalendar calendar = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.calendar, parent, false);
        }
        // Lookup view for data population
        TextView tvCalendar = (TextView) convertView.findViewById(R.id.calendar_display_name);
        TextView tvCalendarSecondary = (TextView) convertView.findViewById(R.id.calendar_type);

        // Populate the data into the template view using the data object
//        tvCalendar.setText(StringUtils.substring(calendar.getDisplayName(), 0, 20).concat("..."));
        tvCalendar.setText(calendar.getDisplayName());
        if(calendar.getAccountType().contains("google")) {
            tvCalendarSecondary.setText(getContext().getResources().getString(R.string.calendar_google));
        } else {
            tvCalendarSecondary.setText(calendar.getDisplayName());
        }

        // Return the completed view to render on screen
        return convertView;
    }
}
