package com.aste.smallapp.calendar.helpers;

import android.content.Context;
import android.text.format.DateFormat;

import org.apache.commons.lang3.Validate;
import org.joda.time.DateTime;

/**
 * Date Helper.
 */
public final class DateHelper {

    /**
     * Convert DateTime to locale printable date (with date and time).
     * @param dateTime
     * @return
     */
    public static String printDateTime(final Context context, final DateTime dateTime) {
        Validate.notNull(dateTime, "DateTime should not be null when trying to print it.");

        java.text.DateFormat formatDate = DateFormat.getLongDateFormat(context);
        java.text.DateFormat formatTime = DateFormat.getTimeFormat(context);
        return formatDate.format(dateTime.toDate()) + " " + formatTime.format(dateTime.toDate());
    }

    /**
     * Convert DateTime to locale printable date (only the date, not the time).
     * @param dateTime
     * @return
     */
    public static String printDate(final Context context, final DateTime dateTime) {
        Validate.notNull(dateTime, "DateTime should not be null when trying to print it.");

        java.text.DateFormat formatDate = DateFormat.getLongDateFormat(context);
        return formatDate.format(dateTime.toDate());
    }
}
