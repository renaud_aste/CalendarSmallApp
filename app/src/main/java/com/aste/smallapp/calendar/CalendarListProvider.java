package com.aste.smallapp.calendar;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;

import com.aste.smallapp.calendar.beans.UserCalendar;

import java.util.ArrayList;
import java.util.List;

/**
 * CalendarSmallApp - Provide the list of calendars.
 */
public class CalendarListProvider {


    public static final String[] EVENT_PROJECTION = new String[]{
            CalendarContract.Calendars._ID,                           // 0
            CalendarContract.Calendars.ACCOUNT_NAME,                  // 1
            CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,         // 2
            CalendarContract.Calendars.OWNER_ACCOUNT,                 // 3
            CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL,         // 4
            CalendarContract.Calendars.ACCOUNT_TYPE,                  // 5
//            CalendarContract.Calendars.IS_PRIMARY,                    // 6
//            CalendarContract.Calendars.VISIBLE,                       // 7
//            CalendarContract.Calendars.NAME                           // 8
    };

    // The indices for the projection array above.
    private static final int PROJECTION_ID_INDEX = 0;
    private static final int PROJECTION_ACCOUNT_NAME_INDEX = 1;
    private static final int PROJECTION_DISPLAY_NAME_INDEX = 2;
    private static final int PROJECTION_OWNER_ACCOUNT_INDEX = 3;
    private static final int PROJECTION_ACCESS = 4;
    private static final int PROJECTION_OWNER_ACCOUNT_TYPE = 5;
//    private static final int PROJECTION_IS_PRIMARY = 6;
//    private static final int PROJECTION_VISIBLE = 7;
//    private static final int PROJECTION_NAME = 8;

    private Context context;

    public CalendarListProvider(Context context) {
        this.context = context;
    }

    public List<UserCalendar> getCalendars() {
        //Run query
        Cursor cur = null;
        ContentResolver cr = context.getContentResolver();
        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        String selection = "(" + CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL + " >= ? )";
        String[] selectionArgs = new String[]{String.valueOf(CalendarContract.Calendars.CAL_ACCESS_CONTRIBUTOR)};

        // Submit the query and get a Cursor object back.
        cur = cr.query(uri, EVENT_PROJECTION, selection, selectionArgs, null);

        // Use the cursor to step through the returned records
        List<UserCalendar> calendars = new ArrayList<>();
        while (cur.moveToNext()) {
            long calID = 0;

            // Get the field values
            calID = cur.getLong(PROJECTION_ID_INDEX);
            String displayName = cur.getString(PROJECTION_DISPLAY_NAME_INDEX);
            String accountName = cur.getString(PROJECTION_ACCOUNT_NAME_INDEX);
            String ownerName = cur.getString(PROJECTION_OWNER_ACCOUNT_INDEX);
            String accountType = cur.getString(PROJECTION_OWNER_ACCOUNT_TYPE);
//            String primary = cur.getString(PROJECTION_IS_PRIMARY);
//            String visible = cur.getString(PROJECTION_VISIBLE);
//            String name = cur.getString(PROJECTION_NAME);
            String access = cur.getString(PROJECTION_ACCESS);

            calendars.add(new UserCalendar(calID, displayName, accountName, ownerName, access, accountType));
        }
        return calendars;
    }

}
