package com.aste.smallapp.calendar.listeners;

import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;

/**
 * Gestion des évênements lors du clic sur la checkbox.
 */
public class OnCheckBoxListener implements CompoundButton.OnCheckedChangeListener {

    /**
     * Bouton pour saisir l'heure de début.
     */
    private Button btTimePickerStart;

    /**
     * Bouton pour saisir l'heure de fin.
     */
    private Button btTimePickerEnd;

    public OnCheckBoxListener(final Button btTimePickerStart, final Button btTimePickerEnd) {
        this.btTimePickerStart = btTimePickerStart;
        this.btTimePickerEnd = btTimePickerEnd;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        btTimePickerStart.setVisibility(isChecked ? View.GONE : View.VISIBLE);
        btTimePickerEnd.setVisibility(isChecked ? View.GONE : View.VISIBLE);
    }
}
