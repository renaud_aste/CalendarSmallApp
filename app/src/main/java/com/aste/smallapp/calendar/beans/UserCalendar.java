package com.aste.smallapp.calendar.beans;

/**
 * CalendarSmallApp - Representation of CalendarContract.Calendars.
 */
public class UserCalendar {

    private long id;

    private String displayName;

    private String accountName;

    private String ownerName;

    private String access;

    private String accountType;

    public UserCalendar(long id, String displayName, String accountName, String ownerName, String access, String accountType) {
        this.id = id;
        this.displayName = displayName;
        this.accountName = accountName;
        this.ownerName = ownerName;
        this.access = access;
        this.accountType = accountType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    @Override
    public String toString() {
        return "UserCalendar{" +
                "id=" + id +
                ", displayName='" + displayName + '\'' +
                ", accountName='" + accountName + '\'' +
                ", ownerName='" + ownerName + '\'' +
                ", accessLevel='" + access + '\'' +
                '}';
    }
}
