package com.aste.smallapp.calendar.listeners;

import android.app.DatePickerDialog;
import android.widget.Button;
import android.widget.DatePicker;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;

/**
 * Created by renaud on 09/07/15.
 */
public class OnDateSetListenerCustom implements DatePickerDialog.OnDateSetListener {

    /**
     * Le callback de la saisie de la date.
     */
    private DateCallback callback;

    /**
     * La date que l'on est en train de renseigner
     */
    private DateTime dateTime;

    public OnDateSetListenerCustom(final DateCallback callback, final DateTime dateTime) {
        this.callback = callback;
        this.dateTime = dateTime;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        this.dateTime = this.dateTime.withYear(year).withMonthOfYear(monthOfYear).withDayOfMonth(dayOfMonth);
        callback.onDateTimeSet(dateTime);
    }
}
