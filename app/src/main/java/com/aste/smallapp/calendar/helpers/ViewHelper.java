package com.aste.smallapp.calendar.helpers;

import android.view.View;
import android.widget.TextView;

/**
 * Helper for Android UI components.
 */
public final class ViewHelper {

    private ViewHelper() {
    }

    public static void setTextViewColor(int color, TextView... textViews) {
        if (textViews != null) {
            for (TextView textView : textViews) {
                textView.setTextColor(color);
            }
        }
    }
}
